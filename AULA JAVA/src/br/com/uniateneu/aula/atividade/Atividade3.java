package br.com.uniateneu.aula.atividade;

// import java.util.Random;
import java.util.Scanner;

public class Atividade3 {
    private static Scanner s = new Scanner(System.in);
    // private static Random random = new Random();

    /**
     * @category Atividade
     * @author Pedro Lucas - 20191119387
     * @author Matheus Gustavo - 20192113862
     * @author Rafael de Oliveira Machado - 20192113816
     */
    public static void main(String[] args) {
        questao01();
        System.out.println();
        questao02();
        System.out.println();
        questao03();
        System.out.println();
        questao04();
        System.out.println();
        questao05();
        System.out.println();
        questao06();
        System.out.println();
        questao07();
        System.out.println();
        questao08();
        System.out.println();
        s.close();
    }

    /**
     * Imprimir os números pares de 10 a 100.
     */
    public static void questao01() {
        System.out.println("Números pares [10, 100]: ");
        for (int i = 10; i <= 100; i++) {
            System.out.print((i % 2 == 0 ? i : "") + " ");
        }
    }

    /**
     * Imprimir a sequência: 11, 21, 31... 101.
     */
    public static void questao02() {
        System.out.println("Sêquencia: ");
        for (int i = 0, j = 11; i < 10; i++, j += 10) {
            System.out.print(j + " ");
        }
        System.out.println();
    }

    /**
     * Ler 10 números reais e imprimir o maior.
     */
    public static void questao03() {
        double max = Double.MIN_VALUE;
        for (int i = 0; i < 10; i++) {
            System.out.printf("Digite um numero real: ");
            // double numero = random.nextDouble() * 100;
            // System.out.println(numero);

            double numero = s.nextDouble();
            max = max > numero ? max : numero;
        }

        System.out.println("Maior valor é: " + max);
    }

    /**
     * Ler 10 números e exibir a soma dos impares.
     */
    public static void questao04() {
        int somaImpares = 0;
        for (int i = 0; i < 10; i++) {
            System.out.printf("Digite um numero: ");
            // int numero = random.nextInt(10);
            // System.out.println(numero);

            int numero = s.nextInt();

            somaImpares += numero % 2 != 0 ? numero : 0;

        }

        System.out.println("Soma dos impares é: " + somaImpares);
    }

    /**
     * Ler 10 números e exibir a soma dos múltiplos de 5.
     */
    public static void questao05() {
        int somaMutiplos5 = 0;
        for (int i = 0; i < 10; i++) {
            System.out.printf("Digite um numero: ");
            // int numero = random.nextInt(100);
            // System.out.println(numero);

            int numero = s.nextInt();

            somaMutiplos5 += numero % 5 == 0 ? numero : 0;

        }

        System.out.println("Soma dos multiplos de 5 é: " + somaMutiplos5);
    }

    /**
     * Ler diversos números e exibir quantos foram digitados. O valor -1 será a
     * condição de parada.
     */
    public static void questao06() {
        int c = 0;
        // int num;
        System.out.println("Digite os numeros: ");
        while (s.nextInt() != -1) { // (num = random.nextInt(10) - 1) != -1
            // System.out.println(num);
            c++;
        }

        System.out.println("Total de numeros digitados: " + c);
    }

    /**
     * Ler um somatório de números inteiros que irá parar ao ser digitado o número 0
     * e calcular a média destes números digitados.
     */
    public static void questao07() {
        int c = 0, num;
        double soma = 0;
        System.out.println("Digite os numeros: ");
        while ((num = s.nextInt()) != 0) { // (num = random.nextInt(10)) != 0
            // System.out.println(num);
            soma += num;
            c++;
        }

        System.out.println("Soma dos numeros digitados: " + soma);
        System.out.printf("Media dos numeros digitados: %.2f\n", c == 0 ? 0 : soma / c);
    }

    /**
     * Leia o genero e a altura de várias pessoas (parando ao encontrar uma altura
     * zero), calcule a média das alturas dos homens e das mulheres, escrever a
     * quantidade de homens e mulheres e qual o grupo é o mais alto.
     */
    public static void questao08() {
        int cHomem = 0, cMulher = 0;
        double hHomem = 0, hMulher = 0;
        // String arr[] = { "M", "F" };
        while (true) {
            System.out.print("Digite o genero [M] [F]: ");
            // String genero = arr[random.nextInt(2)];
            // System.out.println(genero);
            String genero = s.next();

            System.out.print("Digite a altura: ");
            // Double altura = (double) random.nextInt(10);
            // System.out.println(altura);
            Double altura = s.nextDouble();

            if (altura == 0)
                break;

            switch (genero) {
                case "M":
                    hHomem += altura;
                    cHomem++;
                    break;
                case "F":
                    hMulher += altura;
                    cMulher++;
                    break;
            }
        }

        double mHomem = cHomem == 0 ? 0 : hHomem / cHomem, mMulher = cMulher == 0 ? 0 : hMulher / cMulher;
        System.out.printf("Total de homes: %d\n", cHomem);
        System.out.printf("Média altura dos homes: %.2f\n", mHomem);
        System.out.printf("Total de mulheres: %d\n", cMulher);
        System.out.printf("Média altura das mulheres: %.2f\n", mMulher);
        System.out.println("Grupo Mais Alto: "
                + (mHomem == mMulher ? "Ambos possuem a mesma altura" : mHomem > mMulher ? "Homens" : "Mulheres"));
    }
}