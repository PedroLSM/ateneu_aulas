package br.com.uniateneu.aula.tad;

public class Circulo {
    private final float PI = 3.14f;
    public int x;
    public int y;
    public float raio;

    public Circulo(float raio, int x, int y) {
        this.raio = raio;
        this.x = x;
        this.y = y;
    }

    public float area() {
        return PI * raio * raio;
    }
}