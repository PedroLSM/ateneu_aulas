package br.com.uniateneu.aula.atividade.tad;

public class Lista {
    private No cabeca;

    public Lista(int v) {
        this.cabeca = criarNo(v);
    }

    public Lista(int... v) {
        this.cabeca = criarNo(v[0]);
        for (int i = 1; i < v.length; i++) {
            this.inserirNoFinal(v[i]);
        }
    }

    public void inserirNoFinal(int valor) {
        No ultimoNo = this.getUltimoNo();

        ultimoNo.setProximo(criarNo(valor));
    }

    public No getUltimoNo() {
        No ultimoNo = cabeca;
        while (ultimoNo.getProximo() != null) {
            ultimoNo = ultimoNo.getProximo();
        }

        return ultimoNo;
    }

    public No criarNo(int valor) {
        return new No(valor, null);
    }

    public int getTamanhoRecursivo() {
        if (this.cabeca == null)
            return 0;

        return getTamanhoRecursivo(cabeca);
    }

    private int getTamanhoRecursivo(No noInicial) {
        if (noInicial == null)
            return 0;

        return 1 + getTamanhoRecursivo(noInicial.getProximo());
    }

    // OBTER NO

    public No getNoPorIndice(int index) {
        if (index == 0)
            return this.cabeca;

        return getNoRecursivo(this.cabeca.getProximo(), --index);
    }

    private No getNoRecursivo(No no, int index) {
        if (index == 0)
            return no;

        if (no.getProximo() == null)
            return null;

        return getNoRecursivo(no.getProximo(), --index);
    }

    public No getNoPorValor(int valor) {
        if (cabeca.getValor() == valor)
            return cabeca;

        No no = cabeca;
        while (no.getProximo() != null) {
            no = no.getProximo();

            if (no.getValor() == valor)
                return no;
        }

        return null;
    }

    // ALTURA
    public int getAlturaDoNo(No no) {
        if (no == null)
            return 0;

        return getTamanhoRecursivo(no.getProximo());
    }

    // PROFUNDIDADE
    public int getProfundidadeDoNo(No no) {
        if (no == null)
            return 0;

        return getPronfundidadeRecursivo(no);
    }

    private int getPronfundidadeRecursivo(No limit) {
        if (cabeca == null)
            return 0;

        return getPronfundidadeRecursivo(cabeca, limit);
    }

    private int getPronfundidadeRecursivo(No no, No limit) {
        if (no == limit)
            return 0;

        return getPronfundidadeRecursivo(no.getProximo(), limit) + 1;
    }

    // ORDEM
    public String ordemDaLista() {
        if (cabeca.getProximo() == null)
            return "Linear";

        No noA = cabeca;
        No noB = cabeca.getProximo();

        String ordem = "Linear";

        while (noB.getProximo() != null) {
            if (noA.getValor() != noB.getValor()) {
                ordem = noA.getValor() < noB.getValor() ? "Crescente" : "Descrecente";
                break;
            }

            noA = noB;
            noB = noA.getProximo();
        }

        if (ordem.equals("Linear"))
            return "Linear";

        while (noB.getProximo() != null) {
            String atual = noA.getValor() < noB.getValor() ? "Crescente" : "Descrecente";

            if (!atual.equals(ordem))
                return "Esta bagunçada";

            noA = noB;
            noB = noA.getProximo();
        }

        return ordem;
    }

    // CONCATENAR

    public void concatenar(Lista b) {
        No ultimoNo = this.getUltimoNo();

        ultimoNo.setProximo(b.cabeca);
    }

    // SHOW

    public void show() {
        No no = cabeca;
        if (cabeca != null) {
            System.out.print(cabeca + " ");
        }
        while (no.getProximo() != null) {
            no = no.getProximo();
            System.out.print(no + " ");
        }

        System.out.println();
    }
}