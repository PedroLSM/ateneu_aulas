package br.com.uniateneu.aula.tad;

public class Binario {

    public static String Dec2Bin(int n) {
        if (n == 0)
            return "0";

        return Dec2Bin(n / 2) + ("" + n % 2);
    }

    public static String Dec2Bin(long n) {
        if (n == 0)
            return "0";

        return Dec2Bin(n / 2) + ("" + n % 2);
    }

    public static void Dec2Bin(int n, boolean print) {
        if (print) {
            String bin = Dec2Bin(n);
            System.out.println(bin);
        }
    }

    public static void Dec2Bin(long n, boolean print) {
        if (print) {
            String bin = Dec2Bin(n);
            System.out.println(bin);
        }

    }
}