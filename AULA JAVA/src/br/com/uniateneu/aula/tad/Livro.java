package br.com.uniateneu.aula.tad;

import java.util.Arrays;

/**
 * @category Atividade
 * @author Pedro Lucas - 20191119387
 * @author Matheus Gustavo - 20192113862
 * @author Rafael de Oliveira Machado - 20192113816
 */
public class Livro implements Comparable<Livro> {

    String titulo;
    String autor;
    String genero;
    int ano;

    public Livro(String titulo, String autor, String genero, int ano) {

        this.titulo = titulo;
        this.autor = autor;
        this.genero = genero;
        this.ano = ano;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public String getAutor() {
        return this.autor;
    }

    public String getGenero() {
        return this.genero;
    }

    public int getAno() {
        return this.ano;
    }

    public void buscaLivroModerno() {

    }

    public static int verificaNoModernismo(Livro livro) {

        int anoLivro = livro.getAno();

        return anoLivro < 1930 ? -1 : anoLivro <= 1945 ? 0 : 1;
    }

    public String definicaoPeriodo() {
        int periodo = Livro.verificaNoModernismo(this);
        if (periodo == -1)
            return "O livro pertence ao 1º periodo do modernismo";

        if (periodo == 0)
            return "O livro pertence ao 2º periodo do modernismo";

        return "O livro pertence ao 3º periodo do modernismo";
    }

    public static Livro buscaLivroModerno(Livro[] livros, int totalDeLivros, String autor, String genero) {
        Livro[] livrosCp = Livro.ordenaLivros(livros, totalDeLivros);

        Arrays.sort(livrosCp);

        for (int i = livrosCp.length - 1; i >= 0; i--) {
            if (Livro.verificaNoModernismo(livrosCp[i]) != 0)
                continue;

            if (livrosCp[i].autor.equalsIgnoreCase(autor) && livrosCp[i].genero.equalsIgnoreCase(genero))
                return livrosCp[i];
        }

        return null;
    }

    @Override
    public String toString() {
        String retorno = "titulo: " + this.titulo + "\n" + "autor: " + this.autor + "\n" + "genero: " + this.genero
                + "\n" + "ano: " + this.ano + "\n" + "periodo: " + this.definicaoPeriodo();

        return retorno;
    }

    @Override
    public int compareTo(Livro livro) {
        int sortAutor = this.sortAutor(livro);
        int sortGenero = this.sortGenero(livro);
        int sortAnoPublicacao = this.sortAnoPublicacao(livro);

        return sortAutor == 0 ? sortGenero == 0 ? sortAnoPublicacao : sortGenero : sortAutor;
    }

    public static Livro[] ordenaLivros(Livro[] livros, int length) {
        Livro[] livrosCopy = Arrays.copyOf(livros, length);
        Arrays.sort(livrosCopy);
        return livrosCopy;
    }

    private int sortAutor(Livro livro) {
        return this.autor.compareTo(livro.autor);
    }

    private int sortGenero(Livro livro) {
        return this.genero.compareTo(livro.genero);
    }

    private int sortAnoPublicacao(Livro livro) {
        return this.ano == livro.ano ? 0 : this.ano > livro.ano ? 1 : -1;
    }
}
