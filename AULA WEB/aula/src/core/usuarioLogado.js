window.onload = function () {
    const user = localStorage.getItem('username');
    const paths = window.location.pathname.split('/');

    if (!user && paths[paths.length - 1] === 'index.html') {
        window.open('./pages/login.html', '_self');
    } else if (!user) {
        this.console.log('Olá')
        window.open('./login.html', '_self');
    } else {
        const username = this.localStorage.getItem('username');
        const spanUser = this.document.getElementById("user");
        if (spanUser) {
            spanUser.innerHTML = username;
        }
    }
}