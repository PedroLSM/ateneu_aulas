package br.com.uniateneu.aula.fixacao;

public class Fixacao9 {
    public static void main(String[] args) {
        Lista listaA = new Lista(1, 5, 9, 3, 4, 9, 91);

        System.out.println(listaA.getTamanhoInterativa());
        System.out.println(listaA.getTamanhoRecursivo());

        System.out.println(listaA.valorExiste(3));
        System.out.println(listaA.valorExiste(2));

    }
}