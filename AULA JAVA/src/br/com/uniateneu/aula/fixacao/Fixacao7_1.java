package br.com.uniateneu.aula.fixacao;

import java.util.Arrays;
import java.util.Scanner;

/**
 * @category Fixacao
 * @author Pedro Lucas - 20191112387
 * 
 *         1) Escreva um procedimento para ordenar cinco números fornecidos pelo
 *         usuário. Utilize a passagem de parâmetros.
 */
public class Fixacao7_1 {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int[] nums = obterValoresInteiros(5);

        sc.close();

        ordernarCrescente(nums);

        numerosOrdenados(nums);

    }

    private static void numerosOrdenados(int[] nums) {
        System.out.println("Numeros Ordenados");
        System.out.println(Arrays.toString(nums));
    }

    private static int[] obterValoresInteiros(int n) {
        int nums[] = new int[n];

        for (int i = 0; i < nums.length; i++)
            nums[i] = solicitarNumero();

        return nums;
    }

    public static int solicitarNumero() {
        System.out.print("Digite um valor inteiro: ");
        return sc.nextInt();
    }

    public static void ordernarCrescente(int[] array) {
        Arrays.sort(array);
    }

}