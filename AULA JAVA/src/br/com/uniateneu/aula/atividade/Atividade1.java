package br.com.uniateneu.aula.atividade;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

/**
 * @category Atividade
 * @author Pedro Lucas - 20191119387
 * @author Matheus Gustavo - 20192113862
 */
public class Atividade1 {
	private static Scanner s = new Scanner(System.in);

	public static void main(String[] args) {
		separar(25);
		questao01();

		separar(25);
		questao02();

		separar(25);
		questao03();

		separar(25);
		questao04();
	}

	/**
	 * Leia o primeiro nome e a idade de uma pessoa e informe seu nome e seu
	 * possível ano de nascimento.
	 */
	public static void questao01() {
		escreverQuestao(1);
		System.out.print("Digite seu nome: ");
		String nome = s.nextLine();

		System.out.print("Digite sua idade: ");
		int idade = s.nextInt();

		int anoNascimento = anoAtual() - idade;

		System.out.println(nome + " seu provavel ano de nascimento é " + (anoNascimento - 1) + " ou " + anoNascimento);
	}

	/**
	 * @return retorna o ano atual de acordo com o calendario gregoriano.
	 */
	public static int anoAtual() {
		Date date = new Date();
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		int year = calendar.get(Calendar.YEAR);

		return year;
	}

	/**
	 * Ler a massa e aceleração de um corpo qualquer e escrever qual a força deste
	 * corpo através da fórmula F=m8a.
	 */
	public static void questao02() {
		escreverQuestao(2);
		System.out.print("Digite a massa: ");
		double massa = s.nextDouble();

		System.out.print("Digite a aceleracao: ");
		double aceleracao = s.nextDouble();

		double forca = calcularForca(massa, aceleracao);

		System.out.printf("O resultado da força é: %.2f\n", forca);
	}

	/**
	 * 
	 * @param massa
	 * @param aceleracao
	 * @return retorna a força (Forca = massa * aceleracao)
	 */
	public static double calcularForca(double massa, double aceleracao) {
		return massa * aceleracao;
	}

	/**
	 * Ler o salário de um fucionário e imprí-lo com um aumento de 15%.
	 */
	public static void questao03() {
		escreverQuestao(3);
		System.out.print("Digite o salário: ");

		double salario = s.nextDouble();
		double aumento = 1.15;

		double salarioFinal = salario * aumento;

		System.out.printf("Salário final com aumento de 15%%: %.2f\n", salarioFinal);
	}

	/**
	 * Ler um valor real e ecrever apenas a parte decimal deste número lido.
	 */
	private static void questao04() {
		escreverQuestao(4);
		System.out.print("Digite um número real: ");
		double valorReal = s.nextDouble();

		System.out.println("A parte decimal é: " + parteDecimal(valorReal));
	}

	public static String parteDecimal(double valorReal) {
		String[] partes = Double.toString(valorReal).split("\\.");
		return partes.length <= 1 ? "0" : partes[1];
	}

	/**
	 * 
	 * @param n número da questão
	 */
	public static void escreverQuestao(int n) {
		System.out.println("Questão " + n);
	}

	/**
	 * 
	 * @param n numero de repetição do caractere '='
	 */
	public static void separar(int n) {
		System.out.println();
		System.out.println(new String(new char[n]).replace("\0", "="));
	}
}
