package br.com.uniateneu.aula.fixacao;

import java.util.Scanner;

/**
 * @category Fixacao
 * @author Pedro Lucas - 20191112387
 * 
 *         2) Construa um procedimento que verifique, sem utilizar a função mod,
 *         se um número é divisível por outro.
 */
public class Fixacao7_3 {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        int[] nums = obterValoresInteiros(2);

        sc.close();

        boolean ehDivisivel = ehDivisivel(nums[0], nums[1]);

        printarResultadp(nums, ehDivisivel);
    }

    private static void printarResultadp(int[] nums, boolean ehDivisivel) {
        String msg = ehDivisivel ? nums[0] + " é divisivel por " + nums[1]
                : nums[0] + " não é divisivel por " + nums[1];

        System.out.println(msg);
    }

    private static int[] obterValoresInteiros(int n) {
        int nums[] = new int[n];

        for (int i = 0; i < nums.length; i++)
            nums[i] = solicitarNumero();

        return nums;
    }

    public static int solicitarNumero() {
        System.out.print("Digite um valor inteiro: ");
        return sc.nextInt();
    }

    public static boolean ehDivisivel(int a, int b) {
        return a % b == 0;
    }

}