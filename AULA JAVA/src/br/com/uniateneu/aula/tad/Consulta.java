package br.com.uniateneu.aula.tad;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Consulta {
    public String local;
    public Date data;

    public Consulta(String local, String data) {
        this.local = local;
        try {
            this.data = DateFormat.getDateInstance().parse(data);
        } catch (ParseException e) {
            this.data = null;
        }
    }

    public String getDataFormatada() {
        if (this.data == null)
            return "A data informada estava inválida";
        return new SimpleDateFormat("dd/MM/yyyy").format(this.data);
    }

    @Override
    public String toString() {
        String retorno = "Local: " + this.local + "\n" + "Data da Consulta: " + this.getDataFormatada();
        return retorno;
    }
}
