package br.com.uniateneu.aula.tad;

public class Pilha {
    private int[] vetor;
    int top;

    public Pilha(int tamanho) {
        this.vetor = new int[tamanho];
        this.top = -1;
    }

    public void push(int valor) {
        if (top < vetor.length - 1)
            vetor[++top] = valor;
    }

    public int pop() {
        if (top > -1) {
            return vetor[top--];
        }

        return -1;
    }

    public void show() {
        System.out.print("[ ");
        for (int i = 0; i <= top; i++) {
            System.out.print(vetor[i] + " ");
        }
        System.out.print("]");

        System.out.println();
    }

    public static void main(String[] args) {
        Pilha fila = new Pilha(5);

        fila.show();

    }
}