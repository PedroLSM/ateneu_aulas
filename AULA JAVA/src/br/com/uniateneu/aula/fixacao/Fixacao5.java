package br.com.uniateneu.aula.fixacao;

import java.util.Scanner;

import br.com.uniateneu.aula.tad.Aluno;
import br.com.uniateneu.aula.tad.Cachorro;
import br.com.uniateneu.aula.tad.Consulta;
import br.com.uniateneu.aula.tad.Nota;
import br.com.uniateneu.aula.tad.Turma;

/**
 * @category Fixacao
 * @author Pedro Lucas - 20191112387
 * 
 *         1) Criar um TAD para armazenamento de nome, turma, disciplina,
 *         matrícula e notas de alunos. 2) Crie um TAD para armazenamento de
 *         dados de cães para uma clínica veterinária.
 */
public class Fixacao5 {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        questao01();
        System.out.println();
        questao02();
        System.out.println();

        sc.close();
    }

    public static void questao01() {
        System.out.print("Qual o nome da turma: ");
        String nomeTurma = sc.nextLine();

        System.out.print("Qual a quantidade de alunos: ");
        int qtdAluno = sc.nextInt();

        Turma turma = new Turma(nomeTurma, qtdAluno);

        sc.nextLine();
        for (int j = 0; j < turma.alunos.length; j++) {
            Aluno aluno = new Aluno();

            System.out.print("Qual o nome do aluno: ");
            aluno.nome = sc.nextLine();

            System.out.print("Qual a matricula do aluno: ");
            aluno.matricula = sc.nextInt();

            sc.nextLine();
            System.out.println("Qual a quantidade de disciplinas: ");
            int qtd = sc.nextInt();
            sc.nextLine();

            aluno.notas = new Nota[qtd];
            for (int i = 0; i < aluno.notas.length; i++) {
                Nota nota = new Nota();

                System.out.print("Qual o nome da disciplina: ");
                nota.disciplina = sc.nextLine();

                System.out.print("Nota: ");
                nota.nota = sc.nextDouble();

                sc.nextLine();
                aluno.notas[i] = nota;
            }

            turma.alunos[j] = aluno;
        }

        System.out.println();
        System.out.println(turma.toString());
    }

    public static void questao02() {
        System.out.println("Digite o nome do cachorro");
        String nome = sc.nextLine();

        System.out.println("Digite a raça do cachorro");
        String raca = sc.nextLine();

        System.out.println("Digite a idade do cachorro");
        int idade = sc.nextInt();
        sc.nextLine();

        System.out.println("Se o cachorro tiver doença, informe (caso não pressione enter): ");
        String doenca = sc.nextLine();

        Cachorro cachorro = new Cachorro(nome, raca, doenca, idade);
        System.out.println("O " + cachorro.nome + ", realizou alguma consulta? [S/N]");
        char res = sc.nextLine().charAt(0);
        if (res == 's' || res == 'S') {

            System.out.println("Informe o local: ");
            String local = sc.nextLine();

            System.out.println("Informe a data [dd/MM/yyyy]: ");
            String data = sc.nextLine();

            cachorro.ultimaConsulta = new Consulta(local, data);
        }

        System.out.println(cachorro);
    }
}