package br.com.uniateneu.aula.fixacao;

import java.util.Scanner;
import java.util.Arrays;

/**
 * @category Fixacao
 * @author Pedro Lucas
 */
public class Fixacao4 {
    public static void main(String[] args) {
        questao01_1();
        questao01();
        questao02();
    }

    private static void questao02() {
        Scanner sc = new Scanner(System.in);

        System.out.print("Quantos alunos tem na turma: ");
        int numAlunos = sc.nextInt();

        double matrizNotas[][] = new double[numAlunos][5];

        for (int i = 0; i < matrizNotas.length; i++) {
            System.out.println("Aluno " + (i + 1));
            for (int j = 0; j < matrizNotas[0].length; j++) {
                System.out.print("Digite a " + (j + 1) + "º nota: ");
                matrizNotas[i][j] = sc.nextDouble();
            }
        }

        for (int i = 0; i < matrizNotas.length; i++) {
            System.out.println("Aluno " + (i + 1));
            System.out.print("Média Final: ");
            double geral = 0.0;
            for (int j = 0; j < matrizNotas[0].length; j++) {
                geral += matrizNotas[i][j];
            }
            System.out.printf("%.2f\n", geral / matrizNotas[0].length);
        }

        sc.close();
    }

    private static void questao01() {
        Scanner sc = new Scanner(System.in);

        int matriz[][] = new int[3][3];
        System.out.println("Preencendo Matriz 3x3: ");

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print("Digite o valor posicao [" + i + ", " + j + "]: ");
                matriz[i][j] = sc.nextInt();
            }
        }

        for (int[] vetor : matriz) {
            for (int valor : vetor) {
                System.out.print(valor + " ");
            }
            System.out.println();
        }

        sc.close();
    }

    private static void questao01_1() {
        Scanner sc = new Scanner(System.in);

        System.out.print("Digite o total de numeros: ");
        int tamanho = sc.nextInt();

        int[] vetor = new int[tamanho];
        for (int i = 0, j = 10; i < vetor.length; i++, j += 10) {
            vetor[i] = j;
        }

        System.out.println("Sequência: " + Arrays.toString(vetor));

        sc.close();
    }
}