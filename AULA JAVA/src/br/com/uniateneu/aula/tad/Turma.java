package br.com.uniateneu.aula.tad;

public class Turma {
    public String nome;
    public Aluno[] alunos;

    public Turma(String nome, int totalDeAlunos) {
        this.nome = nome;
        this.alunos = new Aluno[totalDeAlunos];
    }

    @Override
    public String toString() {
        String retorno = "Turma: " + this.nome + "\n" + "Alunos: \n";

        for (Aluno aluno : this.alunos) {
            retorno += aluno.toString();
            retorno += "=======\n";
        }

        return retorno;
    }
}