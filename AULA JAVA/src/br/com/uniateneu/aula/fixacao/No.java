package br.com.uniateneu.aula.fixacao;

public class No {
    private int valor;
    private No proximo;

    public No(int valor, No proximo) {
        this.setValor(valor);
        this.setProximo(proximo);
    }

    public No getProximo() {
        return proximo;
    }

    public void setProximo(No proximo) {
        this.proximo = proximo;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        return valor + "";
    }
}