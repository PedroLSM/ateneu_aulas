package br.com.uniateneu.aula.tad;

public class Aluno {
    public String nome;
    public int matricula;
    public Nota[] notas;

    public Aluno(String nome, int matricula) {
        this.nome = nome;
        this.matricula = matricula;

    }

    public Aluno() {
    }

    @Override
    public String toString() {
        String retorno = "Nome: " + this.nome + "\n" + "Matricula: " + this.matricula + "\n" + "Notas: " + "\n";

        for (Nota nota : this.notas) {
            retorno += nota.toString() + "\n";
        }

        return retorno;
    }
}