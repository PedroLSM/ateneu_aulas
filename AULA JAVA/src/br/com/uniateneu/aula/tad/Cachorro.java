package br.com.uniateneu.aula.tad;

public class Cachorro {
    public String nome;
    public String raca;
    public String doenca;
    public int idade;
    public Consulta ultimaConsulta;

    public Cachorro(String nome, String raca, String doenca, int idade) {
        this.doenca = doenca;
        this.idade = idade;
        this.nome = nome;
        this.raca = raca;
    }

    @Override
    public String toString() {
        String retorno = "Nome: " + this.nome + "\n" + "Raça: " + this.raca + "\n" + "Doença: " + this.doenca + "\n"
                + "Idade: " + this.idade + "\n" + (this.ultimaConsulta == null ? "Ainda não realizou consulta"
                        : "Dados da ultima consulta: \n" + this.ultimaConsulta + "\n");
        return retorno;
    }
}