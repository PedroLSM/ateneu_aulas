window.onload = function () {
    localStorage.removeItem('username');
}

function confirmarCadastro() {
    const nome = document.cd.login.value;
    const password = document.cd.password.value;

    const usersString = localStorage.getItem('users') || "[]";
    let users = JSON.parse(usersString);

    if (users.find(u => u.nome === nome && u.password === password)) {
        window.open('../index.html', '_self');
        localStorage.setItem('username', nome);
    } else {
        alert('Usuário e/ou senha inválido.')
        localStorage.removeItem('username');
    }
}