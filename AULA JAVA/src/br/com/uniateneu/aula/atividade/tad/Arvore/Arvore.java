package br.com.uniateneu.aula.atividade.tad.Arvore;

/**
 * @category Atividade
 * @author Pedro Lucas - 20191119387
 * @author Matheus Gustavo - 20192113862
 * @author Rafael de Oliveira Machado - 20192113816
 */
public class Arvore {

    private No raiz;
    private int COUNT = 10;

    /**
     * Criar a arvore.
     */
    public Arvore() {
        this.raiz = null;
    }

    public No getRaiz() {
        return raiz;
    }

    /**
     * Verificar se a arvore esta vazia
     */
    public boolean isEmpyt() {
        if (raiz == null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Retornar a altura da arvore.
     */
    public int getAltura() {
        return this.getAltura(raiz);
    }

    private int getAltura(No pai) {
        if (pai == null) {
            return 0;
        }
        final int alEsq = getAltura(pai.getEsq());
        final int altDir = getAltura(pai.getDir());
        if (alEsq > altDir)
            return pai == raiz ? alEsq : alEsq + 1;
        else
            return pai == raiz ? altDir : altDir + 1;
    }

    /**
     * Contar todos os NOS na árvore.
     * 
     * @param pai
     * @return
     */
    public int countNos() {
        return countNos(raiz);
    }

    private int countNos(No pai) {
        if (pai == null) {
            return 0;
        }
        final int qntEsq = countNos(pai.getEsq());
        final int qntDir = countNos(pai.getDir());
        return (qntEsq + qntDir + 1);
    }

    /**
     * Contar todos os NOS não folha na árvore.
     * 
     * @param pai
     * @return
     */
    public int countNosNaoFolha() {
        return countNosNaoFolha(raiz);
    }

    private int countNosNaoFolha(No pai) {
        if (pai == null || (pai.getEsq() == null && pai.getDir() == null)) {
            return 0;
        }

        final int qntEsq = countNosNaoFolha(pai.getEsq());
        final int qntDir = countNosNaoFolha(pai.getDir());

        return (qntEsq + qntDir + 1);
    }

    /**
     * Contar todos os NOS folha na árvore.
     * 
     * @param pai
     * @return
     */
    public int countNosFolha() {
        return countNosFolha(raiz);
    }

    private int countNosFolha(No pai) {
        if (pai == null) {
            return 0;
        }

        if ((pai.getEsq() == null && pai.getDir() == null)) {
            return 1;
        }

        final int qntEsq = countNosFolha(pai.getEsq());
        final int qntDir = countNosFolha(pai.getDir());

        return (qntEsq + qntDir);
    }

    /**
     * Inserir elementos na Arvore Binaria de Busca. Logo todo no pai possui valores
     * maiores a direita e valores menores a esquerda. Valor repetidos são
     * descatador.
     */

    public void insert(int... valores) {
        for (final int valor : valores) {
            this.insert(valor);
        }
    }

    public boolean insert(int valor) {
        No novo = new No(valor);
        if (raiz == null) {// caso raiz nula insere o novo valor.
            raiz = novo;
            return true;
        } else {// varre a arvore em busca da folha correta.
            No atual = raiz;
            No ant = null;
            while (atual != null) {// Novega na arvore ate ANT encontrar a folha CERTA.
                ant = atual;
                if (valor == atual.getValor()) {// elementos já inseridos são desconciderados.
                    novo = null;
                    return false;
                } else if (valor > atual.getValor())
                    atual = atual.getDir();// se o valor maior que o atual navega para direita.
                else
                    atual = atual.getEsq();// se o valor maior que o atual navega para esquerda.
            }
            if (valor > ant.getValor())
                ant.setDir(novo);
            else
                ant.setEsq(novo);
        }
        return true;
    }

    public boolean deletarArvore() {
        return deletarArvore(raiz);
    }

    private boolean deletarArvore(No no) {
        if (no == null)
            return this.isEmpyt();

        deletarArvore(no.getEsq());
        deletarArvore(no.getDir());

        no.setEsq(null);
        no.setDir(null);

        this.raiz = null;

        return this.isEmpyt();
    }

    /**
     * Remover elementos na Arvore Binaria de Busca.
     * 
     * @param int valor
     * @return boolean
     */
    public void removeNo(int valor) {
        this.removeNo(raiz, valor);
    }

    private No removeAtual(No atual) {
        No aux1, aux2;
        if (atual.getEsq() == null) {
            aux1 = atual.getDir();
            return aux1;
        }

        aux1 = atual;
        aux2 = atual.getEsq();
        while (aux2.getDir() != null) {
            aux1 = aux2;
            aux2 = aux2.getDir();
        }

        if (aux1 != atual) {
            aux1.setDir(aux2.getEsq());
            aux2.setEsq(atual.getEsq());
        }

        aux2.setDir(atual.getDir());
        return aux2;
    }

    private void removeNo(No raiz, int valor) {
        if (raiz == null) {
            System.out.println("A arvore esta vazia!\n");
            return;
        }
        No ant = null;
        No atual = raiz;
        while (atual != null) {
            if (atual.getValor() == valor) {
                if (atual == raiz) {
                    raiz = removeAtual(atual);
                } else {
                    if (ant.getEsq() == atual)
                        ant.setEsq(removeAtual(atual));
                    else
                        ant.setDir(removeAtual(atual));
                }
                return;
            }
            ant = atual;
            if (valor < atual.getValor())
                atual = atual.getEsq();
            else
                atual = atual.getDir();
        }
    }

    /**
     * Buscar elementos na Arvore Binaria de Busca.
     * 
     * @param int valor
     * @return boolean
     */
    public boolean buscar(int valor) {
        if (raiz == null) {
            return false;
        }
        No atual = raiz;
        while (atual != null) {
            if (valor == atual.getValor()) {
                return true;
            } else if (valor > atual.getValor())
                atual = atual.getDir();
            else
                atual = atual.getEsq();
        }
        return false;
    }

    /**
     * Buscar elemento impar na Arvore Binaria de Busca.
     * 
     * @param int valor
     * @return boolean
     */
    public Integer buscarImpar() {
        if (raiz == null) {
            return null;
        }

        return buscarImpar(raiz);
    }

    private Integer buscarImpar(No no) {
        if (no == null)
            return null;

        if (no.isImpar()) {
            return no.getValor();
        }

        final Integer esq = buscarImpar(no.getEsq());
        final Integer dir = buscarImpar(no.getDir());

        return esq == null ? dir == null ? null : dir : esq;
    }

    /**
     * Buscar maior elemento na Arvore Binaria de Busca.
     * 
     * @param int valor
     * @return boolean
     */
    public Integer buscarMaior() {

        return buscarMaior(raiz);
    }

    private Integer buscarMaior(final No no) {
        if (no == null) {
            return null;
        }

        final No dir = no.getDir();

        return dir == null ? no.getValor() : buscarMaior(dir);
    }

    public boolean ehArvoreBinariaDeBusca() {
        return ehArvoreBinariaDeBusca(raiz);
    }

    private boolean ehArvoreBinariaDeBusca(final No no) {
        if (no == null)
            return false;

        if (!no.isBinariaBusca()) {
            return false;
        }

        ehArvoreBinariaDeBusca(no.getEsq());
        ehArvoreBinariaDeBusca(no.getDir());

        return true;
    }

    /**
     * Referencia - GeeksForGeeks
     * 
     * LINK: https://www.geeksforgeeks.org/print-binary-tree-2-dimensions/
     */
    private void show2DUtil(final No root, int space) {
        if (root == null)
            return;

        space += COUNT;

        show2DUtil(root.getDir(), space);

        System.out.print("\n");
        for (int i = COUNT; i < space; i++)
            System.out.print(" ");
        System.out.print(root.getValor() + "\n");

        show2DUtil(root.getEsq(), space);
    }

    private void show2D(final No root) {
        show2DUtil(root, 0);
    }

    public void show2D() {
        show2D(raiz);
    }

    public static void main(final String[] args) {
        final Arvore tree = new Arvore();
        tree.insert(2, 8, 4, 1, 5, 9, 3, 7, 10, 53);
        // tree.insert(2, 4, 8, 10, 12, 60);

        tree.show2D();

        System.out.println("===============");

        questao01(tree);

        questao02(tree);

        questao04(tree);

        questao03(tree);

    }

    private static void questao04(final Arvore tree) {
        System.out.print("Arvoré binária de busca: ");
        System.out.println(tree.ehArvoreBinariaDeBusca());
    }

    private static void questao03(final Arvore tree) {
        System.out.print("Arvoré binária limpa: ");
        System.out.println(tree.deletarArvore());
        System.out.println("Número de nós: " + tree.countNos());
    }

    private static void questao02(final Arvore tree) {
        System.out.print("Buscar número impar na arvoré binária: ");
        System.out.println(tree.buscarImpar());

        System.out.print("Buscar maior numero na arvoré binária: ");
        System.out.println(tree.buscarMaior());
    }

    private static void questao01(final Arvore tree) {
        System.out.print("Número de nós: ");
        System.out.println(tree.countNos());

        System.out.print("Número de nós não folha: ");
        System.out.println(tree.countNosNaoFolha());

        System.out.print("Número de nós folha: ");
        System.out.println(tree.countNosFolha());

        System.out.print("Altura da arvoré binária: ");
        System.out.println(tree.getAltura());
    }
}