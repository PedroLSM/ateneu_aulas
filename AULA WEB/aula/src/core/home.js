window.onload = function () {
    const user = localStorage.getItem('username');

    const list = document.querySelector('#nav');
    if (!user) {
        const values = [{ label: 'Entrar', href: './pages/login.html' }, { label: 'Cadastrar', href: './pages/cadastrar.html' }]
        for (const value of values) {
            const li = document.createElement('li');
            li.className = 'parent';

            const a = document.createElement('a');
            a.className = 'link';
            a.href = value.href;

            const text = document.createTextNode(value.label);

            a.appendChild(text);
            li.appendChild(a);
            list.appendChild(li);
        }
    } else {
        const li = document.createElement('li');
        const text = document.createTextNode('Olá, ' + user);

        li.appendChild(text);
        list.appendChild(li);

        const li2 = document.createElement('li');
        li2.className = 'parent';

        const a = document.createElement('a');
        a.className = 'link';
        a.href = './pages/login.html';

        const text2 = document.createTextNode('Sair');

        a.appendChild(text2);
        li2.appendChild(a);
        list.appendChild(li2);
    }
}