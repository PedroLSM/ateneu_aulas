package br.com.uniateneu.aula.tad;

public class Nota {
    public String disciplina;
    public double nota;

    @Override
    public String toString() {
        String retorno = "Disciplina: " + this.disciplina + ", Nota: " + this.nota;
        return retorno;
    }
}