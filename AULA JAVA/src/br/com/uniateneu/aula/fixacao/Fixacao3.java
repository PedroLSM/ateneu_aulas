package br.com.uniateneu.aula.fixacao;

import java.util.Scanner;

/**
 * @category Fixicao
 * @author Pedro Lucas - 20191112387
 */
public class Fixacao3 {

    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {

        questao01();
        System.out.println();

        questao02();
        System.out.println();

        questao03();
        System.out.println();
    }

    /**
     * Leia 50 valores diferentes e imprima o maior.
     */
    private static void questao01() {
        // public static Scanner sc = new Scanner(System.in);

        int max = 0;
        for (int i = 0; i < 5; i++) {
            System.out.print("Digite um numero: ");
            max = Math.max(sc.nextInt(), max);
        }

        System.out.println("O maior numero é: " + max);
    }

    /**
     * Faça o somatório dos números impares de 0 a 150.
     */
    private static void questao02() {
        // public static Scanner sc = new Scanner(System.in);

        int soma = 0;
        for (int i = 0; i <= 150; i++) {
            soma += i % 2 != 0 ? i : 0;
        }

        System.out.println("Soma dos impares de 0 a 150: " + soma);
    }

    /**
     * Faça um programa para identificar se um número é primo. Lembre-se que número
     * primo, é um número natural, maior que 1, apenas divisível por si próprio e
     * pela unidade.
     */
    private static void questao03() {
        // public static Scanner sc = new Scanner(System.in);

        System.out.print("Digite um numero: ");
        int n = sc.nextInt();
        boolean ehPrimo = ehPrimo(n);

        System.out.println(n + " " + (ehPrimo ? "é primo" : "não é primo"));
    }

    /**
     * Verificar se @param n é um numero primo.
     * 
     * @param n numero que será analisado
     * @return true se o numero for primo ou false caso contrário.
     */
    private static boolean ehPrimo(int n) {
        for (int i = 2; i < n; i++) {
            if (n % i == 0)
                return false;
        }

        return true;
    }
}