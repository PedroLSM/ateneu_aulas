package br.com.uniateneu.aula.fixacao;

import br.com.uniateneu.aula.tad.Binario;

public class Fixacao8 {
    public static void main(String[] args) {
        String binario = Binario.Dec2Bin(12);
        System.out.println(binario);

        binario = Binario.Dec2Bin(11111111111L);
        System.out.println(binario);

        Binario.Dec2Bin(0, true);
        Binario.Dec2Bin(4L, true);

        Binario.Dec2Bin(11111111111L, true);
    }
}