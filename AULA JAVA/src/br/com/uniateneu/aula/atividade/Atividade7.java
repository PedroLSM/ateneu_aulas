package br.com.uniateneu.aula.atividade;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.Scanner;

/**
 * @category Atividade
 * @author Pedro Lucas - 20191119387
 * @author Matheus Gustavo - 20192113862
 * @author Rafael de Oliveira Machado - 20192113816
 */
public class Atividade7 {
    private static Scanner sc = new Scanner(System.in);
    private static Random random = new Random();

    public static void main(String[] args) {
        separar(30);
        questao01();

        separar(30);
        questao02();

        separar(30);
        questao03();
    }

    private static void questao01() {
        escreverQuestao(1);

        int vals[] = obterValoresInteiros(2);

        int mdc = mdc(vals[0], vals[1]);

        sc.nextLine();

        System.out.println(mdc);
    }

    public static int mdc(int a, int b) {
        int resto = a % b;
        return resto == 0 ? b : mdc(b, resto);
    }

    public static void questao02() {
        escreverQuestao(2);

        System.out.println("Sorteando 1000 números no intervalo [0, 100]...");

        int vals[] = obterValoresAleatorio(1000);

        sortear(vals);

        numeroSorteadoMaisVezes(vals);

        numeroSorteadoMenosVezes(vals);

        maiorNumero(vals);

        menorNumero(vals);

        // mostarValores(vals);
    }

    public static void mostarValores(int[] vals) {
        System.out.println("============");
        System.out.println(Arrays.toString(vals));
        System.out.println("============");
    }

    public static void numeroSorteadoMaisVezes(int[] vals) {
        int maiorRepeticao = vals[0], countMaior = Integer.MIN_VALUE;

        int index = 0;
        for (int j = index; j < vals.length;) {

            int count = 0;
            for (; index < vals.length; index++) {
                count += vals[index] == vals[j] ? 1 : 0;

                if (vals[index] != vals[j])
                    break;
            }

            if (count > countMaior) {
                maiorRepeticao = vals[j];
                countMaior = count;
            }

            j = index;
        }

        System.out.println(
                "O número sorteado mais vezes foi " + maiorRepeticao + ", total de repetições foi " + countMaior);
    }

    public static void numeroSorteadoMenosVezes(int[] vals) {
        int menorRepeticao = vals[0], countMenor = Integer.MAX_VALUE;

        int index = 0;
        for (int j = index; j < vals.length;) {

            int count = 0;
            for (; index < vals.length; index++) {
                count += vals[index] == vals[j] ? 1 : 0;

                if (vals[index] != vals[j])
                    break;
            }

            if (count < countMenor) {
                menorRepeticao = vals[j];
                countMenor = count;
            }

            j = index;
        }

        System.out.println(
                "O número sorteado menos vezes foi " + menorRepeticao + ", total de repetições foi " + countMenor);
    }

    public static void maiorNumero(int[] vals) {
        System.out.println("O maior valor sorteado é " + vals[vals.length - 1]);
    }

    public static void menorNumero(int[] vals) {
        System.out.println("O menor valor sorteado é " + vals[0]);
    }

    public static void sortear(int vals[]) {
        Arrays.sort(vals);
    }

    public static int[] obterValoresInteiros(int n) {
        int nums[] = new int[n];

        for (int i = 0; i < nums.length; i++)
            nums[i] = solicitarNumero(i + 1);

        return nums;
    }

    public static int[] obterValoresAleatorio(int n) {
        int nums[] = new int[n];

        for (int i = 0; i < nums.length; i++)
            nums[i] = solicitarAleatorio(100);

        return nums;
    }

    public static int solicitarNumero(int n) {
        System.out.printf("Digite o %dº valor: ", n);
        return sc.nextInt();
    }

    public static void questao03() {
        escreverQuestao(3);

        System.out.print("Digite a data inicial [dd/mm/yyyy]: ");
        Date dataInicio = obterData();

        System.out.print("Digite a data final [dd/mm/yyyy]: ");
        Date dataFinal = obterData();

        long dtInicioTime = obterTempoEmMilisegundos(dataInicio);
        long dtFimTime = obterTempoEmMilisegundos(dataFinal);
        long milis = Math.abs(dtInicioTime - dtFimTime);

        long horas = milisegundosParaHoras(milis);

        System.out.printf("Total de horas: %d horas", horas);
    }

    private static long milisegundosParaHoras(long milis) {
        return (((milis / 1000) / 60) / 60);
    }

    private static long obterTempoEmMilisegundos(Date dataInicio) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(dataInicio);
        return cal.getTimeInMillis();
    }

    private static Date obterData() {
        while (true) {
            try {
                Date dataInicio = DateFormat.getDateInstance().parse(sc.nextLine());
                return dataInicio;
            } catch (ParseException e) {
                System.out.println("Informar uma data válida.");
            }
        }
    }

    public static int solicitarAleatorio(int max) {
        return random.nextInt(max + 1);
    }

    public static void escreverQuestao(int n) {
        System.out.println("Questão " + n);
        System.out.println();
    }

    public static void separar(int n) {
        System.out.println();
        System.out.println(new String(new char[n]).replace("\0", "="));
        System.out.println();
    }
}