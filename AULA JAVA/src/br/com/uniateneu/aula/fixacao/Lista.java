package br.com.uniateneu.aula.fixacao;

public class Lista {
    private No cabeca;

    public Lista(int v) {
        this.cabeca = criarNo(v);
    }

    public Lista(int... v) {
        this.cabeca = criarNo(v[0]);
        for (int i = 1; i < v.length; i++) {
            this.inserirNoFinal(v[i]);
        }
    }

    public void inserirNoFinal(int valor) {
        No ultimoNo = this.getUltimoNo();

        ultimoNo.setProximo(criarNo(valor));
    }

    public No getUltimoNo() {
        No ultimoNo = cabeca;
        while (ultimoNo.getProximo() != null) {
            ultimoNo = ultimoNo.getProximo();
        }

        return ultimoNo;
    }

    public No criarNo(int valor) {
        return new No(valor, null);
    }

    public int getTamanhoInterativa() {
        if (this.cabeca == null)
            return 0;

        int i = 1;
        No no = this.cabeca;
        while (no.getProximo() != null) {
            no = no.getProximo();
            i++;
        }

        return i;
    }

    public int getTamanhoRecursivo() {
        if (this.cabeca == null)
            return 0;

        return getTamanhoRecursivo(cabeca);
    }

    private int getTamanhoRecursivo(No noInicial) {
        if (noInicial == null)
            return 0;

        return 1 + getTamanhoRecursivo(noInicial.getProximo());
    }

    public boolean valorExiste(int valor) {
        No no = cabeca;
        while (no.getProximo() != null && no.getValor() != valor) {
            no = no.getProximo();
        }

        return no.getValor() == valor;
    }
}