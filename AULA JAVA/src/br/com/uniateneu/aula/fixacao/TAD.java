package br.com.uniateneu.aula.fixacao;

import br.com.uniateneu.aula.tad.Circulo;

public class TAD {
    public static void main(String[] args) {
        Circulo circulo = new Circulo(2, 0, 0);

        float area = circulo.area();

        System.out.println(area);
    }
}