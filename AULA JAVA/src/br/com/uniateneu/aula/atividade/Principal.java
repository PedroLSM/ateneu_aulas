package br.com.uniateneu.aula.atividade;

import java.util.Scanner;

import br.com.uniateneu.aula.tad.Livro;

/**
 * @category Atividade
 * @author Pedro Lucas - 20191119387
 * @author Matheus Gustavo - 20192113862
 * @author Rafael de Oliveira Machado - 20192113816
 */
public class Principal {

    public static void main(String[] args) {

        // Livro[] livros = obterDadosEstatico();
        Livro[] livros = obterDadosDoUsuario();

        for (Livro livro : livros) {
            System.out.println(livro);
            System.out.println("==========");
        }

        // ORDERNANDO ARRAY DE LIVROS CRESCENTE
        System.out.println("ORDERNANDO ARRAY DE LIVROS CRESCENTE");
        System.out.println("==========");
        livros = Livro.ordenaLivros(livros, livros.length);
        for (Livro livro : livros) {
            System.out.println(livro);
            System.out.println("==========");
        }

        // BUSCA LIVRO
        // buscarLivroEstatico(livros);
        buscarLivroDinamico(livros);
    }

    private static void buscarLivroEstatico(Livro[] livros) {
        System.out.println("BUSCAR LIVRO MAIS NOVO 2º PERIODO MORDERNISMO: Vinicius de Morais - poesia");
        System.out.println("==========");

        String nomeAutor = "Vinicius de Morais";
        String nomeGenero = "poesia";
        Livro livro = Livro.buscaLivroModerno(livros, livros.length, nomeAutor, nomeGenero);

        System.out.println(livro == null ? "Nenhum Livro encontrado" : livro);
    }

    private static void buscarLivroDinamico(Livro[] livros) {
        Scanner sc = new Scanner(System.in);

        System.out.println("BUSCAR LIVRO MAIS NOVO 2º PERIODO MORDERNISMO: ");
        System.out.println("==========");
        System.out.print("Autor: ");
        String nomeAutor = sc.nextLine();
        System.out.print("Genero: ");
        String nomeGenero = sc.nextLine();
        Livro livro = Livro.buscaLivroModerno(livros, livros.length, nomeAutor, nomeGenero);

        System.out.println(livro == null ? "Nenhum Livro encontrado" : livro);

        sc.close();
    }

    private static Livro[] obterDadosEstatico() {
        Livro[] livros = { new Livro("Novos Poemas", "Vinicius de Morais", "poesia", 1938),
                new Livro("Poemas Escritos na India", "Cecilia Meireles", "poesia", 1962),
                new Livro("Orfeu da Conceição", "Vinicius de Morais", "teatro", 1954),
                new Livro("Ariana, a Mulher", "Vinicius de Morais", "poesia", 1936) };

        return livros;
    }

    private static Livro[] obterDadosDoUsuario() {
        Scanner sc = new Scanner(System.in);

        Livro[] livros = new Livro[1];
        for (int i = 0; i < livros.length; i++) {
            sc.nextLine();
            System.out.println("Informe o titulo do LIVRO:");
            String titulo = sc.nextLine();
            System.out.println("Informe o autor do LIVRO");
            String autor = sc.nextLine();
            System.out.println("Informe o genero do LIVRO");
            String genero = sc.nextLine();
            System.out.println("Informe o ano do LIVRO");
            int ano = sc.nextInt();

            Livro livro = new Livro(titulo, autor, genero, ano);

            livros[i] = livro;
        }

        sc.close();

        return livros;
    }
}
