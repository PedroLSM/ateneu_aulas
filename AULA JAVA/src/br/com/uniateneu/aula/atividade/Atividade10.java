package br.com.uniateneu.aula.atividade;

import java.util.Random;
import java.util.Scanner;

import br.com.uniateneu.aula.atividade.tad.Pilha;

/**
 * @category Atividade
 * @author Pedro Lucas - 20191119387
 * @author Matheus Gustavo - 20192113862
 * @author Rafael de Oliveira Machado - 20192113816
 */
public class Atividade10 {
    private static Scanner sc = new Scanner(System.in);
    private static Random random = new Random();

    public static void main(String[] args) {
        questao01();

        questao02();

        questao03();
    }

    private static void questao03() {
        System.out.println("Escreva a Expressão: ");
        String exp = sc.nextLine();

        Pilha<String> pilhaChars = new Pilha<String>();
        String[] parenteses = exp.replaceAll("[^(|^)]", "").split("");

        System.out.println(pilhaChars.checarExpressao(parenteses) ? "Expressão Válida" : "Expressão Inválida");
        System.out.println();
    }

    private static void questao02() {
        System.out.println("Escreva uma frase: ");
        String frase = sc.nextLine();

        Pilha<Character> pilhaChars = new Pilha<Character>();
        char[] chars = frase.toCharArray();
        for (char c : chars) {
            pilhaChars.push(c);
        }

        Pilha<String> pilhaString = new Pilha<String>();
        String[] letras = frase.split(" ");
        for (String l : letras) {
            pilhaString.push(" ");
            pilhaString.push(l);
        }

        System.out.println("Revertendo Caracteres: ");
        while (!pilhaChars.vazia()) {
            System.out.print(pilhaChars.pop());
        }

        System.out.println();

        System.out.println("Revertendo Palavras: ");
        while (!pilhaString.vazia()) {
            System.out.print(pilhaString.pop());
        }

        System.out.println();
        System.out.println();
    }

    private static void questao01() {
        int nums[] = new int[100];

        for (int i = 0; i < nums.length; i++) {
            nums[i] = random.nextInt(100);
        }

        Pilha<Integer> pilha = new Pilha<Integer>();
        for (int i : nums) {
            if (i % 2 == 0)
                pilha.push(i);
        }

        pilha.show();
        System.out.println();
    }
}