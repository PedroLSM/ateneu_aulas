package br.com.uniateneu.aula.atividade;

import br.com.uniateneu.aula.atividade.tad.Lista;
import br.com.uniateneu.aula.atividade.tad.No;

/**
 * @category Atividade
 * @author Pedro Lucas - 20191119387
 * @author Matheus Gustavo - 20192113862
 * @author Rafael de Oliveira Machado - 20192113816
 */
public class Atividade9 {
    public static void main(String[] args) {
        Lista listaA = new Lista(1, 5, 9, 10, 15, 20, 91);
        Lista listaB = new Lista(91, 20, 15, 10, 9, 5, 1);
        Lista listaC = new Lista(5, 5, 5, 5, 5, 5, 5);

        No no = listaA.getNoPorIndice(2);

        System.out.printf("Altura do 3º nó: %d\n", listaA.getAlturaDoNo(no));

        System.out.printf("Profundidade do 3º nó: %d\n", listaA.getProfundidadeDoNo(no));

        System.out.printf("Tamanho da Lista: %d\n", listaA.getTamanhoRecursivo());

        No no2 = listaA.getNoPorIndice(2);
        System.out.println("Obtendo no por indice (2): " + no2);

        No no3 = listaA.getNoPorValor(91);
        System.out.println("Obtendo no por valor (91): " + no3);

        System.out.println("Ordem da lista: " + listaA.ordemDaLista());
        System.out.println("Ordem da lista: " + listaB.ordemDaLista());
        System.out.println("Ordem da lista: " + listaC.ordemDaLista());

        listaA.concatenar(listaB);
        listaA.show();
    }
}