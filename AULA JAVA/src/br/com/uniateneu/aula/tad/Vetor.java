package br.com.uniateneu.aula.tad;

import java.util.Arrays;
import java.util.Random;

public class Vetor {
    public int[] vetor;

    public Vetor(int len) {
        vetor = new int[len];
    }

    public void preencherAleatorio() {
        Random random = new Random();
        for (int i = 0; i < this.vetor.length; i++) {
            this.vetor[i] = random.nextInt(100);
        }
    }

    public int obterMaiorValor() {
        Arrays.sort(vetor);

        return vetor[vetor.length - 1];
    }

    public int obterMaiorValor(boolean segundoMaior) {
        Arrays.sort(vetor);

        return segundoMaior ? vetor[vetor.length - 2] : vetor[vetor.length - 1];
    }

}