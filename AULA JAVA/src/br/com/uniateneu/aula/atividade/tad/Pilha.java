package br.com.uniateneu.aula.atividade.tad;

import java.util.Arrays;

/**
 * @category Atividade
 * @author Pedro Lucas - 20191119387
 * @author Matheus Gustavo - 20192113862
 * @author Rafael de Oliveira Machado - 20192113816
 */
public class Pilha<T> {
    private T[] vetor;
    int top;

    public Pilha(int tamanho) {
        this.vetor = (T[]) new Object[tamanho];
        this.top = -1;
    }

    public Pilha() {
        this.vetor = (T[]) new Object[10];
        this.top = -1;
    }

    public void push(T valor) {
        if (top < vetor.length - 1)
            vetor[++top] = valor;

        if (top >= vetor.length - 1)
            vetor = Arrays.copyOf(vetor, top + vetor.length);
    }

    public T pop() {
        if (top > -1) {
            return vetor[top--];
        }

        return null;
    }

    public void show() {
        System.out.print("[ ");

        for (int i = 0; i <= top; i++) {
            System.out.print(vetor[i] + " ");
        }

        System.out.println("]");
    }

    public boolean checarExpressao(T[] parenteses) {
        for (T c : parenteses) {
            if (((String) c).equals("(")) {
                this.push(c);
                continue;
            }

            if (this.vazia())
                return false;

            this.pop();
        }

        return vazia();
    }

    public boolean vazia() {
        return this.top == -1;
    }
}