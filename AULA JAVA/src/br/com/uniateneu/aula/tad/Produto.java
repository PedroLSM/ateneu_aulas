package br.com.uniateneu.aula.tad;

public class Produto {
    private int codigo;
    public String nome;
    public double valor;

    public int getCodigo() {
        return codigo;
    }

    public String getNome() {
        return nome;
    }

    public double getValor() {
        return valor;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    @Override
    public String toString() {
        String retorno = "Código: " + this.codigo + "\n" + "Nome: " + this.nome + "\n" + "Valor: "
                + String.format("%.2f", this.valor);

        return retorno;
    }
}