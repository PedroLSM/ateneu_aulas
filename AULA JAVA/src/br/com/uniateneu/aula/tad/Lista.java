package br.com.uniateneu.aula.tad;

public class Lista {
    private No cabeca;

    public Lista(int v) {
        this.cabeca = criarNo(v);
    }

    public Lista(int... v) {
        this.cabeca = criarNo(v[0]);
        for (int i = 1; i < v.length; i++) {
            this.inserirNoFinal(v[i]);
        }
    }

    public void inserirNoFinal(int valor) {
        No ultimoNo = this.getUltimoNo();

        ultimoNo.setProximo(criarNo(valor));
    }

    public No getUltimoNo() {
        No ultimoNo = cabeca;
        while (ultimoNo.getProximo() != null) {
            ultimoNo = ultimoNo.getProximo();
        }

        return ultimoNo;
    }

    public No criarNo(int valor) {
        return new No(valor, null);
    }

    // Concatenar Lista
    public void concatenar(Lista b) {
        No ultimoNo = this.getUltimoNo();

        ultimoNo.setProximo(b.cabeca);
    }

    public int getTamanhoInterativa() {
        int i = cabeca != null ? 1 : 0;

        No no = cabeca;
        while (no.getProximo() != null) {
            no = no.getProximo();
            i++;
        }

        return i;
    }

    public int getTamanhoRecursivo() {
        if (cabeca == null)
            return 0;

        return getTamanhoRecursivo(0, cabeca);
    }

    private int getTamanhoRecursivo(int i, No noInicial) {
        if (noInicial == null)
            return i;

        return getTamanhoRecursivo(++i, noInicial.getProximo());
    }

    public No buscarPosicao(int pos) {
        pos -= cabeca != null ? 1 : 0;

        No no = cabeca;
        while (no.getProximo() != null && pos > 0) {
            no = no.getProximo();
            pos--;
        }

        if (pos != 0)
            throw new NullPointerException("Posição não encontrada");

        return no;
    }

    public boolean valorExiste(int valor) {
        No no = cabeca;
        while (no.getProximo() != null && no.getValor() != valor) {
            no = no.getProximo();
        }

        return no.getValor() == valor;
    }

    public void mudar(int valorAntigo, int valorNovo) {
    }

    public void apagar() {

    }

    public void show() {
        No no = cabeca;
        if (cabeca != null) {
            System.out.print(cabeca + " ");
        }
        while (no.getProximo() != null) {
            no = no.getProximo();
            System.out.print(no + " ");
        }

        System.out.println();
    }

    public static void main(String[] args) {
        Lista listaA = new Lista(1, 5, 9, 3, 4);
        System.out.println(listaA.getTamanhoInterativa());
        System.out.println(listaA.getTamanhoRecursivo());

        Lista listaB = new Lista(91, 8, 7);
        listaA.concatenar(listaB);

        System.out.println(listaA.getTamanhoInterativa());
        System.out.println(listaA.getTamanhoRecursivo());

        System.out.println(listaA.buscarPosicao(2));
        System.out.println(listaA.valorExiste(5));

        listaA.show();
        System.out.println("FIM");
    }
}