window.onload = function () {
    localStorage.removeItem('username');
}

let nome = 'pedro';
let password = '1234';

function confirmarCadastro() {
    const nome = document.cd.login.value;
    const password = document.cd.password.value;

    const usersString = localStorage.getItem('users') || "[]";
    let users = JSON.parse(usersString);

    if (!nome || !password) {
        alert('Informe o usuário e/ou senha.')
        return;
    }

    const model = { nome, password };

    users = [...users, model]
    localStorage.setItem('users', JSON.stringify(users));

    window.open('login.html', '_self');
}