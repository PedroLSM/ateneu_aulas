package br.com.uniateneu.aula.atividade;

import java.util.Scanner;

/**
 * @category Atividade
 * @author Pedro Lucas - 20191119387
 * @author Matheus Gustavo - 20192113862
 * @author Rafael de Oliveira Machado - 20192113816
 */
public class Atividade2 {
    private static Scanner s = new Scanner(System.in);
    private static int[] fibonnacis;

    public static void main(String[] args) {
        questao01();
        questao04();
        questao05();
        questao06();
        questao07();
        questao08();
        questao09();
        questao10();
    }

    /**
     * Escreva um programa que imprima todos os números inteiros pares entre 0 e 36.
     */
    public static void questao01() {
        separar(25);
        escreverQuestao(1);

        for (int i = 0; i <= 36; i++) {
            if (i % 2 == 0) {
                System.out.print(i + " ");
            }
        }

        System.out.println();
    }

    /**
     * Escreva um programa que imprima os 20 primeiros números de Fibonacci.
     */
    public static void questao04() {
        separar(25);
        escreverQuestao(4);

        int numerosFib = 20;
        fibonnacis = new int[numerosFib];

        for (int i = 0; i < numerosFib; i++) {
            System.out.print(fibonnaci(i) + " ");

            // fibonnacis[i] = fibonnaciComMemorizacao(i);
            // System.out.print(fibonnacis[i] + " ");
        }

        System.out.println();
    }

    /**
     * Algoritmo de fibonnaci utilizando recursividade.
     * 
     * @param x o indice do fibonnaci
     * @return o valor do fibonnaci no indice @param x
     */
    public static int fibonnaci(int x) {
        return x <= 1 ? x : fibonnaci(x - 2) + fibonnaci(x - 1);
    }

    /**
     * Algoritmo de fibonnaci utilizando recursividade e memorização.
     * 
     * @param x o indice do fibonnaci
     * @return o valor do fibonnaci no indice @param x
     */
    public static int fibonnaciComMemorizacao(int x) {
        if (x <= 1) {
            fibonnacis[x] = x;
            return x;
        }

        return (fibonnacis[x - 2] == 0 ? fibonnaci(x - 2) : fibonnacis[x - 2])
                + (fibonnacis[x - 1] == 0 ? fibonnaci(x - 1) : fibonnacis[x - 1]);
    }

    /**
     * Ler duas idades e escrever qual delas é a maior.
     */
    public static void questao05() {
        separar(25);
        escreverQuestao(5);
        System.out.print("Digite a 1º idade: ");
        int idade1 = s.nextInt();

        System.out.print("Digite a 2º idade: ");
        int idade2 = s.nextInt();

        System.out.println("A maior idade é: " + Math.max(idade1, idade2));
    }

    /**
     * Entrar com um valor inteiro e dizer a qual mês do ano pertence.
     */
    public static void questao06() {
        separar(25);
        escreverQuestao(6);
        System.out.print("Digite um número de 1 a 12: ");
        int mes = s.nextInt();

        System.out.println("O mês escolhido é: " + obterMes(mes));
    }

    /**
     * 
     * @param mes indice correspondete ao mes do calendario
     * @return o mes do calendario referente ao indice @param mes
     */
    public static String obterMes(int mes) {
        switch (mes) {
            case 1:
                return "JANEIRO";
            case 2:
                return "FEVEREIRO";
            case 3:
                return "MARÇO";
            case 4:
                return "ABRIL";
            case 5:
                return "MAIO";
            case 6:
                return "JUNHO";
            case 7:
                return "JULHO";
            case 8:
                return "AGOSTO";
            case 9:
                return "SETEMBRO";
            case 10:
                return "OUTUBRO";
            case 11:
                return "NOVEMBRO";
            case 12:
                return "DEZEMBRO";
            default:
                return "INVÁLIDO";
        }
    }

    /**
     * Ler duas notas fazer a média destas e escrever se o aluno foi aprovado ou
     * reprovado (média de aprovação igual ou maior que 7).
     */
    public static void questao07() {
        separar(25);
        escreverQuestao(7);
        System.out.print("Digite a 1º nota: ");
        double n1 = s.nextDouble();

        System.out.print("Digite a 2º nota: ");
        double n2 = s.nextDouble();

        double media = (n1 + n2) / 2;

        System.out.printf("Sua média é %.2f você esta %s\n", media, (media >= 7 ? "Aprovado" : "Reprovado"));
    }

    /**
     * Ler 3 lados do triângulo e dizer se este existe e se ele é escaleno,
     * equilátero ou isósceles.
     */
    public static void questao08() {
        separar(25);
        escreverQuestao(8);
        System.out.print("Digite o 1º lado do triângulo: ");
        double a = s.nextDouble();

        System.out.print("Digite o 2º lado do triângulo: ");
        double b = s.nextDouble();

        System.out.print("Digite o 3º lado do triângulo: ");
        double c = s.nextDouble();

        System.out.println(TipoTriangulo(a, b, c));
    }

    /**
     * 
     * @param a lado a do triângulo
     * @param b lado b do triângulo
     * @param c lado c do triângulo
     * @return tipo de triângulo de acordo com os valores dos lados fornecidos
     */
    public static String TipoTriangulo(double a, double b, double c) {
        if (a != b && a != c && b != c) {
            return "Triângulo Escaleno";
        } else if (a == b && a == c) {
            return "Triângulo Equilátero";
        } else {
            return "Triângulo Isóceles";
        }
    }

    /**
     * Leia um ponto qualquer com as coordenadas (x,y) de um plano cartesiano e
     * dizer em qual quadrante ele está situado, se está em um dos eixos ou
     * localizado na origem.
     */
    public static void questao09() {
        separar(25);
        escreverQuestao(9);
        System.out.print("Digite a coordenada x: ");
        double x = s.nextDouble();

        System.out.print("Digite a coordenada y: ");
        double y = s.nextDouble();

        String quadrante = indentificarQuadrante(x, y);

        System.out.println("O ponte x e y (" + x + ", " + y + ") está localizado no(a): " + quadrante);
    }

    /**
     * 
     * @param x coordenada x
     * @param y coordenada y
     * @return o quadrante que está localizado o ponto na coordenada x e y
     */
    public static String indentificarQuadrante(double x, double y) {
        if (x == 0 && y == 0)
            return "Origem";

        if (x == 0 && y != 0)
            return "Eixo Y";

        if (y == 0 && x != 0)
            return "Eixo X";

        if (x > 0) {
            if (y > 0)
                return "1º Quadrante";
            return "4º Quadrante";
        } else {
            if (y > 0)
                return "2º Quadrante";
            return "3º Quadrante";
        }
    }

    /**
     * Construa um programa para calcular as raízes de uma equação do 2o grau a
     * partir dos seus coeficientes A, B e C. Lembre-se da possibilidade de raízes
     * iguais e de não haves raízes real.
     */
    public static void questao10() {
        separar(25);
        escreverQuestao(10);
        System.out.print("Digite o valor de a: ");
        double a = s.nextDouble();

        System.out.print("Digite o valor de b: ");
        double b = s.nextDouble();

        System.out.print("Digite o valor de c: ");
        double c = s.nextDouble();

        String resultado = calcularEquacao2Grau(a, b, c);
        System.out.println(resultado);
    }

    /**
     * equação 2º grau = a²x + bx + c
     * 
     * @param a valor de a
     * @param b valor de b
     * @param c valor de c
     * @return o resultado de x¹ e x² da equação com as devidas validações
     */
    public static String calcularEquacao2Grau(double a, double b, double c) {
        double delta = Math.pow(b, 2) + (-4 * a * c);

        if (delta < 0)
            return "Não possui solução real";

        if (delta == 0) {
            double x = -b / (2 * a);
            return "x¹ e x² = " + x;
        } else {
            double x1 = (-b + Math.sqrt(delta)) / (2 * a);
            double x2 = (-b - Math.sqrt(delta)) / (2 * a);

            return "x¹ = " + x1 + ", x² = " + x2;
        }
    }

    /**
     * 
     * @param n número da questão
     */
    public static void escreverQuestao(int n) {
        System.out.println("Questão " + n);
    }

    /**
     * 
     * @param n numero de repetição do caractere '='
     */
    public static void separar(int n) {
        System.out.println();
        System.out.println(new String(new char[n]).replace("\0", "="));
    }
}