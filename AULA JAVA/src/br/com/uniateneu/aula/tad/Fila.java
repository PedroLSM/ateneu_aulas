package br.com.uniateneu.aula.tad;

import java.util.Arrays;

public class Fila {
    private int[] vetor;
    int p;
    int u;

    public Fila(int tamanho) {
        this.vetor = new int[tamanho];

        Arrays.fill(this.vetor, -1);

        this.p = -1;
        this.u = -1;
    }

    public void enqueue(int valor) {
        if (u < vetor.length - 1)
            vetor[++u] = valor;
    }

    public int dequeue() {
        if (p < vetor.length - 1) {
            int retorno = vetor[++p];

            for (int i = 0; i < vetor.length; i++) {
                vetor[i] = i < u ? vetor[i + 1] : -1;
            }

            --u;
            return retorno;
        }

        Arrays.fill(this.vetor, -1);

        p = -1;
        u = -1;

        return -1;
    }

    public void show() {
        System.out.println(Arrays.toString(vetor));
    }

    public static void main(String[] args) {
        Fila fila = new Fila(5);

        fila.show();

        fila.enqueue(1);
        fila.enqueue(2);
        fila.enqueue(3);
        fila.enqueue(4);
        fila.enqueue(5);

        fila.show();

        fila.dequeue();
        fila.show();
        fila.dequeue();
        fila.show();
        fila.dequeue();
        fila.show();
        fila.dequeue();
        fila.show();
        fila.dequeue();
        fila.show();

        fila.enqueue(9);
        fila.show();
    }
}