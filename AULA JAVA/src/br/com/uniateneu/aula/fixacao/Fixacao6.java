package br.com.uniateneu.aula.fixacao;

import java.util.Scanner;

import br.com.uniateneu.aula.tad.Cachorro;
import br.com.uniateneu.aula.tad.Consulta;

/**
 * @category Fixacao
 * @author Pedro Lucas - 20191112387
 * 
 *         2) Crie um TAD para armazenamento de dados de cães para uma clínica
 *         veterinária.
 */
public class Fixacao6 {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
        questao02();

        sc.close();
    }

    public static void questao02() {
        System.out.println("Digite o nome do cachorro");
        String nome = sc.nextLine();

        System.out.println("Digite a raça do cachorro");
        String raca = sc.nextLine();

        System.out.println("Digite a idade do cachorro");
        int idade = sc.nextInt();
        sc.nextLine();

        System.out.println("Se o cachorro tiver doença, informe (caso não pressione enter): ");
        String doenca = sc.nextLine();

        Cachorro cachorro = new Cachorro(nome, raca, doenca, idade);
        System.out.println("O " + cachorro.nome + ", realizou alguma consulta? [S/N]");
        char res = sc.nextLine().charAt(0);
        if (res == 's' || res == 'S') {

            System.out.println("Informe o local: ");
            String local = sc.nextLine();

            System.out.println("Informe a data [dd/MM/yyyy]: ");
            String data = sc.nextLine();

            cachorro.ultimaConsulta = new Consulta(local, data);
        }

        System.out.println(cachorro);
    }
}