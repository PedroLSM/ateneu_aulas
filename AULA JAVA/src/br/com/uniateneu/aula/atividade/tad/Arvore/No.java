package br.com.uniateneu.aula.atividade.tad.Arvore;

/**
 * @category Atividade
 * @author Pedro Lucas - 20191119387
 * @author Matheus Gustavo - 20192113862
 * @author Rafael de Oliveira Machado - 20192113816
 */
public class No {

    private No esq;
    private No dir;
    private int valor;

    public No(int valor) {
        this.valor = valor;
    }

    public No getEsq() {
        return esq;
    }

    public void setEsq(No esq) {
        this.esq = esq;
    }

    public No getDir() {
        return dir;
    }

    public void setDir(No dir) {
        this.dir = dir;
    }

    public int getValor() {
        return valor;
    }

    public void setValor(int valor) {
        this.valor = valor;
    }

    public boolean isImpar() {
        return this.valor % 2 != 0;
    }

    public boolean isBinariaBusca() {
        if (this.esq == null) {
            if (this.dir == null) {
                return true;
            }

            return this.dir.getValor() > this.getValor();
        }

        if (this.dir == null) {
            return this.esq.getValor() < this.getValor();
        }

        return this.esq.getValor() < this.dir.getValor() && this.dir.getValor() > this.getValor();
    }
}