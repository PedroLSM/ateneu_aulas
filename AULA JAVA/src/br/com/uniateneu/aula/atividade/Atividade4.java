package br.com.uniateneu.aula.atividade;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * @category Atividade
 * @author Pedro Lucas - 20191119387
 * @author Matheus Gustavo - 20192113862
 * @author Rafael de Oliveira Machado - 20192113816
 */
public class Atividade4 {
    private static Scanner s = new Scanner(System.in);
    private static Random random = new Random();

    public static void main(String[] args) {
        questao01();
        System.out.println();
        questao02();
        System.out.println();
        questao03();
        System.out.println();
        questao04();
        System.out.println();
        questao05();
        System.out.println();
        questao06();
        System.out.println();
    }

    public static void questao01() {
        System.out.println();
        int vetor[] = new int[30];

        for (int i = 0; i < vetor.length; i++) {
            vetor[i] = i * i;
        }

        System.out.println(Arrays.toString(vetor));
    }

    public static void questao02() {
        System.out.println();
        double vetorImpares[] = new double[15];

        System.out.println("Digite 15 valores: ");
        for (int i = 0; i < vetorImpares.length; i++) {
            vetorImpares[i] = s.nextDouble();

            // vetorImpares[i] = random.nextInt(10);
            // System.out.println(vetorImpares[i]);
        }

        mostraApenasImpares(vetorImpares);
    }

    public static void questao03() {
        System.out.println();
        System.out.println("Em um vetor de N posições");
        System.out.print("Qual o valor de N: ");
        int n = s.nextInt();

        // int n = random.nextInt(5);
        // System.out.println(n);

        int val[] = new int[n];
        int val2[] = new int[n];

        System.out.println("Digite os " + n + " valores: ");
        for (int i = 0; i < val.length; i++) {
            val[i] = s.nextInt();

            // val[i] = random.nextInt(10);
            // System.out.println(val[i]);

            val2[i] = val[i] * 2;
        }

        System.out.println("Vetor: " + Arrays.toString(val));
        System.out.println("Vetor com dobro do valor: " + Arrays.toString(val2));

    }

    public static void questao04() {
        System.out.println();
        System.out.println("Em um vetor de N posições");
        System.out.print("Qual o valor de N: ");
        int n = s.nextInt();

        // int n = random.nextInt(101);
        // System.out.println(n);

        int val[] = new int[n];

        System.out.println("Digite os " + n + " valores: ");
        for (int i = 0; i < val.length; i++) {
            val[i] = s.nextInt();

            // val[i] = random.nextInt(10);
            // System.out.println(val[i]);
        }

        System.out.println("Vetor Inicial: ");
        System.out.println(Arrays.toString(val));

        System.out.println("Invertendo Posicoes: ");
        for (int i = 0; i < n; i++) {
            int a = random.nextInt(n);
            int b = random.nextInt(n);

            int swap = val[a];
            val[b] = val[a];
            val[a] = swap;
        }

        System.out.println(Arrays.toString(val));
    }

    public static void questao05() {
        System.out.println();
        double vetorImpares[] = new double[15];

        System.out.println("Digite 15 valores: ");
        for (int i = 0; i < vetorImpares.length; i++) {
            vetorImpares[i] = s.nextDouble();

            // vetorImpares[i] = random.nextInt(10);
            // System.out.println(vetorImpares[i]);
        }

        mostraApenasImparesInvertido(vetorImpares);
    }

    public static void questao06() {
        System.out.println();
        System.out.print("Quantos alunos tem na turma: ");
        int numAlunos = s.nextInt();

        // int numAlunos = random.nextInt(10);
        // System.out.println(numAlunos);

        double notas[] = new double[numAlunos];

        for (int i = 0; i < numAlunos; i++) {
            System.out.print("Nota do " + (i + 1) + "º Aluno: ");
            notas[i] = s.nextDouble();

            // notas[i] = random.nextInt(10);
            // System.out.println(notas[i]);
        }

        System.out.println("Melhores Notas: ");
        Arrays.sort(notas);
        if (notas.length > 0) {
            System.out.println(notas[notas.length - 1]);
            for (int i = notas.length - 2; i >= 0; i--) {
                if (notas[i] != notas[i + 1]) {
                    System.out.println(notas[i]);
                    break;
                }
            }
        }
    }

    public static void mostraApenasImpares(double[] vetorImpares) {
        System.out.println("Somente as Posicoes Impares: ");
        for (int i = 0; i < vetorImpares.length; i++) {
            if (i % 2 != 0) {
                System.out.print(vetorImpares[i] + " - ");
            }
        }
    }

    public static void mostraApenasImparesInvertido(double[] vetorImpares) {
        System.out.println("Somente as Posicoes Impares Apartir da Ultima: ");
        for (int i = vetorImpares.length - 1; i >= 0; i--) {
            if (i % 2 != 0) {
                System.out.print(vetorImpares[i] + " - ");
            }
        }
    }
}
