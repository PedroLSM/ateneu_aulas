package br.com.uniateneu.aula.atividade;

import java.util.Arrays;

import br.com.uniateneu.aula.tad.Pessoa;

/**
 * @category Atividade
 * @author Pedro Lucas - 20191119387
 * @author Matheus Gustavo - 20192113862
 * @author Rafael de Oliveira Machado - 20192113816
 */
public class Atividade8 {
    public static void main(String[] args) {
        int resultadoPotencia = calcularPotencia(26, 1);
        System.out.println(resultadoPotencia);

        double resultadoEquacao1Grau = calcularEquacao(10, 100);
        System.out.println(resultadoEquacao1Grau);

        double[] resultadoEquacao2Grau = calcularEquacao(1, -5, 6);
        System.out.println(Arrays.toString(resultadoEquacao2Grau));

        System.out.println();

        for (int i = 0; i < 20; i++) {
            System.out.print(fibonnaci(i) + "  ");
        }

        System.out.println();

        Pessoa pessoa = new Pessoa();

        pessoa.setNome("Fulano");
        pessoa.setTelefone("08599999999");
        pessoa.setEndereco("Rua Fulaninho Fuleus");

        System.out.println(pessoa);

        double[] resultadoEquacaoInvalida = calcularEquacao(1, -5, 6, 8);
        System.out.println(Arrays.toString(resultadoEquacaoInvalida));

    }

    /**
     * 
     * @param x valor da base
     * @param y valor do expoente
     * @return retorna a potencia de x ^ y usando recursividade
     */
    public static int calcularPotencia(int x, int y) {
        if (y == 0)
            return 1;

        if (y == 1)
            return x;

        return x * calcularPotencia(x, --y);
    }

    /**
     * Retornar erro se exister menos de 2 ou mais de 3 coeficientes
     */
    public static double[] calcularEquacao(int... coeficientes) {
        if (coeficientes.length == 2) {
            double raizes[] = { calcularEquacao(coeficientes[0], coeficientes[1]) };
            return raizes;
        }

        if (coeficientes.length == 3) {
            return calcularEquacao(coeficientes[0], coeficientes[1], coeficientes[2]);
        }

        throw new IllegalArgumentException("Funcinalidade disponivel apenas para equação do 1º e 2º grau");
    }

    /**
     * 
     * @param a valor de a da equacao
     * @param b valor de b da equacao
     * @param c valor de c da equacao
     * @return as raizes existentes na equacao do 2º grau
     */
    public static double[] calcularEquacao(double a, double b, double c) {
        double delta = delta(a, b, c);
        ;
        if (delta > 0) {
            double x1 = (-b + delta) / (2 * a);
            double x2 = (-b - delta) / (2 * a);

            double raizes[] = { x1, x2 };

            return raizes;
        }

        if (delta == 0) {
            double x = -b / (2 * a);

            double raizes[] = { x };

            return raizes;
        }

        return null;
    }

    /**
     * 
     * @param a valor de a da equacao
     * @param b valor de b da equacao
     * @param c valor de c da equacao
     * @return valor de delta da equacao do 2º grau
     */
    private static double delta(double a, double b, double c) {
        return Math.pow(b, 2) - (4 * a * c);
    }

    /**
     * 
     * @param a valor de a na equacao
     * @param b valor de b na equacao
     * @return valor do x da equacao do 1º grau
     */
    public static double calcularEquacao(double a, double b) {
        return -b / a;
    }

    /**
     * Algoritmo de fibonnaci utilizando recursividade.
     * 
     * @param x o indice do fibonnaci
     * @return o valor do fibonnaci no indice @param x
     */
    public static int fibonnaci(int x) {
        return x <= 1 ? x : fibonnaci(x - 2) + fibonnaci(x - 1);
    }
}