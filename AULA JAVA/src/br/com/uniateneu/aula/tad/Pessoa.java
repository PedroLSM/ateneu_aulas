package br.com.uniateneu.aula.tad;

public class Pessoa {
    private String nome;
    private String telefone;
    private String endereco;

    public String getEndereco() {
        return endereco;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    @Override
    public String toString() {
        String retorno = "Nome: " + this.nome + "\n" + "Telefone: " + this.telefone + "\n" + "Endereço: "
                + this.endereco;

        return retorno;
    }

}